﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Library
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            BookSQl booksql = new BookSQl();
              dataGridView1.DataSource = booksql.showBooks(booksql.sqlConnection());
             

        }

        private void button1_Click(object sender, EventArgs e)
        {
            BookSQl booksql = new BookSQl();
            string title;
            title = textBox2.Text;
            int PublicationDateBook;
            PublicationDateBook = 1 ;

            Book book = new Book();
            Author author = new Author();
            author.Name = "Чехов";
            book.AddAuthor(author);

            booksql.insert(booksql.sqlConnection(),title,PublicationDateBook);
            dataGridView1.DataSource = booksql.showBooks(booksql.sqlConnection());
        }
    }
}
