﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Library
{
    class BookSQl
    {
        string select = "select * from Books";
        string insertSt = "insert into Books" + "(TitleBook,PublicationDateBook) values(@TitleBook,@PublicationDateBook)";
        string sql = "Data Source=MY_COMPUTER;Initial Catalog=Library;Integrated Security=True";


        public SqlConnection sqlConnection()
        {
            SqlConnection sqlc = new SqlConnection(sql);
            sqlc.Open();

            return sqlc;
        }
        public Book[] showBooks(SqlConnection sqlc)
        {
            
            Book book = new Book();
            
            SqlDataAdapter adapter = new SqlDataAdapter(select,sqlc);
            DataSet set = new DataSet();
            adapter.Fill(set,"Books");
            DataTable datatable = set.Tables[0];
            Book[] books = new Book[datatable.Rows.Count];
            DataRow row;

            for (int i = 0; i < datatable.Rows.Count; i++)
            {

                book = new Book();
                row = datatable.Rows[i];
                book.Id = (int) row["idBook"];
                book.PublicationDateBook = (int) row["PublicationDateBook"];
                book.TitleBook = (string) row["TitleBook"];

                books[i] = book;   

            }

            return books;

        }
        public int insert(Book book)
        {

            List<Author> authors = book.Authors;

            foreach (Author author in authors)
            {
                "insert " + book.Id + author.Id;
            }

            SqlCommand sqlCommand = new SqlCommand(insertSt, sqlc);
            SqlParameter param = new SqlParameter();  
            param.ParameterName = "@TitleBook";
            param.Value =title ;
            param.SqlDbType = SqlDbType.Text;
            sqlCommand.Parameters.Add(param);
            param = new SqlParameter();

            param.ParameterName = "@PublicationDateBook";
            param.Value = publicationDateBook;
            param.SqlDbType = SqlDbType.Int;
            sqlCommand.Parameters.Add(param);

            try
            {
                sqlCommand.ExecuteNonQuery();
            }
            catch (SqlException es)
            {
               // MessageBox.Show(es.Message);
              //  return;
            }

            sqlc.Close();

            return 1;
        }


    }
}
