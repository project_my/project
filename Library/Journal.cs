﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class Journal
    {
        int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        DateTime issueDateBook;

        public DateTime IssueDateBook
        {
            get { return issueDateBook; }
            set { issueDateBook = value; }
        }
        DateTime returnDateBook;

        public DateTime ReturnDateBook
        {
            get { return returnDateBook; }
            set { returnDateBook = value; }
        }
        int idBook;

        public int IdBook
        {
            get { return idBook; }
            set { idBook = value; }
        }


    }
}
