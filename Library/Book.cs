﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library
{
    class Book
    {
        private int idBook;

        public Book()
        {
            Authors = new List<Author>();
        }

        public List<Author> Authors { get; set; }

        public void AddAuthor(Author author)
        {
            Authors.Add(author);
        }
        
        public int Id
        {
            get { return idBook; }
            set { idBook = value; }
        }
        string titleBook;

        public string TitleBook
        {
            get { return titleBook; }
            set { titleBook = value; }
        }
        int publicationDateBook;

        public int PublicationDateBook
        {
            get { return publicationDateBook; }
            set { publicationDateBook = value; }
        }


    }
}
